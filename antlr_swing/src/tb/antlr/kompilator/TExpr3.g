tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer currentScope = 1; //Global scope
}

prog   
  : (e+=zakres | e+=expr | d+=decl)* -> 
  program(name={$e},deklaracje={$d});

zakres 
  : ^(BEGIN {currentScope = locals.enterScope();} (e+=zakres | e+=expr | d+=decl)* {currentScope = locals.leaveScope();} ) -> 
  blok(wyr={$e},dekl={$d});

decl  :
        ^(VAR i1=ID) {locals.newSymbol($ID.text);} -> dek(n={$ID.text+currentScope})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr
        : ^(PLUS  e1=expr e2=expr)  -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)  -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)  -> mnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr)  -> dziel(p1={$e1.st},p2={$e2.st})
        | ID                        -> id(n={$ID.text+locals.searchForSymbol($ID.text)})
        | ^(PODST i1=ID   e2=expr)  -> podstaw(id={$i1.text+locals.searchForSymbol($ID.text)}, val={$e2.st})
        | INT                       -> int(i={$INT.text})
    ;
    